import React, { Component } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { connect } from "react-redux";
import createHistory from 'history/createBrowserHistory';

import Dashboard from './scenes/dashboard/Dashboard';
import Help from './scenes/Help/HelpPage';
import Onboarding from './scenes/onbording/Onboarding';
import NotFound from './scenes/NotFoundPage/NotFoundPage';
import Phonebook from './scenes/phonebook/Phonebook';
import HaederBar from './shared/components/HeaderBar/HeaderBar';

import './App.scss';

const history = createHistory();

class App extends Component {
  
  render() {
    const {userName} = this.props;
    let message = 'Welcome to Voter Circle phonebook'
    if(userName){
      message = `Hello ${userName}`;
    }
    return (
      <main className="app-container">
        <HaederBar title={message} />
        <Router history={history}>
          <Switch>
            <Route exact path="/" component={Onboarding} />
            <Route path="/help" component={Help} />
            <Route path="/dashboard" component={Dashboard} />
            <Route path="/phonebook" component={Phonebook} />
            <Route component={NotFound} />
          </Switch>
        </Router>
      </main>

    );
  }
}

function mapStateToProps(state) {
  return {
    userName: state.userDetails.userName
  };
}
export default connect(mapStateToProps)(App);