import { combineReducers } from 'redux';
import userDetails from './userDetails';
import contactList from './contactList';
import stepsInfo from './stepInfo';

export default combineReducers({
  userDetails, 
  contactList,
  stepsInfo
})