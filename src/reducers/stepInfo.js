
const defaultStateSteps = {
  activeStep: 0,
  steps: [
    {
      id: 1,
      label: ""
    },
    {
      id: 2,
      label: ""
    }
  ]
}
const stepsInfo = (state = defaultStateSteps, action) => {
  switch (action.type) {
    case 'CHANGE_ACTIVE_STEP':
      return {
        ...state,
        activeStep: action.stepIndex
      }
    default:
      return state
  }
}

export default stepsInfo