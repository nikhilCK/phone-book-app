import { Categories } from '../actions';

const contactList = (state = [], action) => {
  switch (action.type) {
    case 'SET_CONTACT_LIST':
      return action.contactList.map((contactInfo, index) => {
        return {
          id:contactInfo.id,
          firstName : contactInfo.first_name,
          lastName: contactInfo.last_name,
          email:contactInfo.email,
          phoneNo: contactInfo.phone,
          location: contactInfo.location,
          age : contactInfo.age,
          categoryId: contactInfo.categoryId ? contactInfo.categoryId : Categories.UNIDENTIFIED.ID
        }
      });
    case 'CHANGE_RELATION':
      return state.map((contactInfo, index) => {
        return contactInfo.id === action.contactId
          ? { ...contactInfo, relationId: action.relationId, categoryId: action.categoryId }
          : contactInfo;
      });
    default:
      return state;
  }
};

export default contactList;
