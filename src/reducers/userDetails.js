const userDetails = (state = {}, action) => {
  switch (action.type) {
    case 'SET_NAME':
      return {
        ...state,
        userName : action.text
      }
    default:
      return state
  }
}

export default userDetails