import React, { Component } from 'react';
import './styles.scss';
import Pane from '../../shared/components/Pane/Pane';
import ButtonIcon from '../../shared/components/Buttons/ButtonIcon/ButtonIcon';
import { Link } from 'react-router-dom';
import ProgressSteps from '../../shared/components/ProgressSteps/ProgressSteps';
import { getContactList } from './Service';
import { connect } from "react-redux";
import { setName, setContactList, changeActiveStep } from '../../actions';

class Onboarding extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false
    }
  }

  toggleLoading = (val) => {
    this.setState({
      isLoading: val
    })
  }
  onFetchSuccess = (contactList) =>{
    this.props.setContactList(contactList);
    this.toggleLoading(false);
    this.props.changeActiveStep(1);
    this.props.history.push('/dashboard');
  }
  fetchContactList = (social_id) => {
    this.toggleLoading(true);
    let userName = prompt("Please enter your name OR Continue with default name", 'Harry Potter');
    if (!userName) {
      userName = "Harry Potter";
    }
    // FIXMe store this in redux store
    this.props.setUserName(userName);
    switch (social_id) {
      case 'google':
        getContactList('google').then(this.onFetchSuccess);
        break;
      case 'facebook':
        getContactList('facebook').then(this.onFetchSuccess);
        break;
      case 'contact_file':
        getContactList('contact_file').then(this.onFetchSuccess);
        break;
      default:
        break;
    }
  }
  render() {
    const {stepsInfo} = this.props;

    let buttonStyle = {
      width: '100px',
      height: '100px',
      backgroundColor: 'white'
    };
    let iconStyle = {
      fontSize: '40px'
    };
    return (
      <main id="oboarding-container">
        <div className="content">
          <ProgressSteps stepList={stepsInfo.steps} activeStepIndex={stepsInfo.activeStep} />
          <div className="top-sec-panel">
            <div className="title">Import your contacts </div>
            <div className="action"><Link to="/help">Help?</Link></div>
          </div>
          <Pane
            header={'Select a platform to import contacts'}
            isLoading={this.state.isLoading}
          >
            <ButtonIcon
              onClick={() => { this.fetchContactList('google') }}
              title={'Google'}
              customStyle={buttonStyle}
              icon={<i className="fa fa-facebook-official" style={iconStyle} aria-hidden="true" />}
            />
            <ButtonIcon
              onClick={() => { this.fetchContactList('facebook') }}
              title={'Facebook'}
              customStyle={buttonStyle}
              icon={<i className="fa fa-google" style={iconStyle} aria-hidden="true" />}
            />
            <ButtonIcon
              onClick={() => { this.fetchContactList('contact_file') }}
              title={'Contact File'}
              customStyle={buttonStyle}
              icon={<i className="fa fa-address-card" style={iconStyle} aria-hidden="true" />}
            />
          </Pane>
        </div>
      </main>
    );
  }
}

function mapStateToProps(state) {
  return {
    userName: state.userDetails.userName,
    stepsInfo: state.stepsInfo
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    setUserName: (userName) => dispatch(setName(userName)),
    changeActiveStep: (index) => dispatch(changeActiveStep(index)),
    setContactList: (contactList) => dispatch(setContactList(contactList))
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Onboarding);
