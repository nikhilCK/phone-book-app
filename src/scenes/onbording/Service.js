export function getContactList(social_login_id){

  return new Promise(function (resolve, reject){
    setTimeout(()=>{
      if(social_login_id === 'google'){
        resolve(googleContactList);
      }else if(social_login_id === 'facebook'){
        resolve(facebookContactList);
      }else if(social_login_id === 'contact_file'){
        resolve(cardContactList);
      }
    },2000)
  })
}

const googleContactList = [
  {
    id:1,
    first_name : 'nikhil',
    last_name: 'chougle',
    email:'nikhil@gmail.com',
    phone: '3242536544',
    location: 'kol',
    age : {
      min: 20,
      max: 30
    }
  },
  {
    id:2,
    first_name : 'nikhil 1',
    last_name: 'chougle 1',
    email:'chougle@gmail.com',
    phone: '1234567890',
    location: 'pune',
    age : {
      min: 15,
      max: 20
    }
  },
  {
    id:3,
    first_name : 'shardul',
    last_name: 'karajgar',
    email:'shardul@gmail.com',
    phone: '9080706012',
    location: 'bangalore',
    age : {
      min: 41,
      max: 50
    }
  },
  {
    id:4,
    first_name : 'venki',
    last_name: 'chougle',
    email:'venki@gmail.com',
    phone: '657377235',
    location: 'mum',
    age : {
      min: 10,
      max: 15
    }
  },
  {
    id:5,
    first_name : 'koyal',
    last_name: 'singh',
    email:'koyal@gmail.com',
    phone: '3242536544',
    location: 'ca',
    age : {
      min: 20,
      max: 30
    }
  },
  {
    id:6,
    first_name : 'norman',
    last_name: 'rage',
    email:'norman@gmail.com',
    phone: '653735563',
    location: 'LA',
    age : {
      min: 20,
      max: 25
    }
  }
]

const facebookContactList = [
  {
    id:5,
    first_name : 'nikhil1',
    last_name: 'chougle1',
    email:'nikhil@gmail.com',
    phone: '3242536544',
    location: 'kol',
    age : {
      min: 20,
      max: 30
    }
  },
  {
    id:6,
    first_name : 'nikhil 2',
    last_name: 'chougle 2',
    email:'chougle@gmail.com',
    phone: '1234567890',
    location: 'pune',
    age : {
      min: 15,
      max: 20
    }
  },
  {
    id:7,
    first_name : 'shardul 1',
    last_name: 'karajgar 1',
    email:'shardul@gmail.com',
    phone: '9080706012',
    location: 'bangalore',
    age : {
      min: 41,
      max: 50
    }
  },
  {
    id:8,
    first_name : 'venki 1',
    last_name: 'chougle 1',
    email:'venki@gmail.com',
    phone: '657377235',
    location: 'mum',
    age : {
      min: 10,
      max: 15
    }
  },
  {
    id:9,
    first_name : 'koyal 1',
    last_name: 'singh 1',
    email:'koyal@gmail.com',
    phone: '3242536544',
    location: 'ca',
    age : {
      min: 20,
      max: 30
    }
  },
  {
    id:10,
    first_name : 'norman 1',
    last_name: 'rage 1',
    email:'norman@gmail.com',
    phone: '653735563',
    location: 'LA',
    age : {
      min: 20,
      max: 25
    }
  }
]

const cardContactList = [
  {
    id:18,
    first_name : 'nikhil 3',
    last_name: 'chougle 3',
    email:'nikhil@gmail.com',
    phone: '3242536544',
    location: 'kol',
    age : {
      min: 20,
      max: 30
    }
  },
  {
    id:19,
    first_name : 'nikhil 3',
    last_name: 'chougle 3',
    email:'chougle@gmail.com',
    phone: '1234567890',
    location: 'pune',
    age : {
      min: 15,
      max: 20
    }
  },
  {
    id:17,
    first_name : 'shardul 3',
    last_name: 'karajgar 3',
    email:'shardul@gmail.com',
    phone: '9080706012',
    location: 'bangalore',
    age : {
      min: 41,
      max: 50
    }
  },
  {
    id:10,
    first_name : 'venki 3',
    last_name: 'chougle 3',
    email:'venki@gmail.com',
    phone: '657377235',
    location: 'mum',
    age : {
      min: 10,
      max: 15
    }
  },
  {
    id:14,
    first_name : 'koyal 3',
    last_name: 'singh 3',
    email:'koyal@gmail.com',
    phone: '3242536544',
    location: 'ca',
    age : {
      min: 20,
      max: 30
    }
  },
  {
    id:12,
    first_name : 'norman 3',
    last_name: 'rage 3',
    email:'norman@gmail.com',
    phone: '653735563',
    location: 'LA',
    age : {
      min: 20,
      max: 25
    }
  }
]