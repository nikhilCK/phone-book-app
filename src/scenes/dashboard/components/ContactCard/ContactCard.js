
import React from "react";
import PropTypes from 'prop-types';

import "./styles.scss"
import { Relations } from '../../../../actions';


const ContactCard = (props) => {

  const onClick = (e) => {
    e.preventDefault();
    e.target.blur();
    props.onClick(props.id);
  };
  const selectable = !(props.relationId) ? 'selectable' : '';
  return (
    <div className={`crd-usr-contact ${selectable}`} onClick={onClick}>
      <div className="crd-left">
        <p className="crd-usr-name">
          {props.firstName + ' ' + props.lastName}
        </p>
        <p className="crd-usr-details">
          <span className="crd-usr-email"><i className="fa fa-envelope" aria-hidden="true"></i> {props.email}</span>
          <span className="crd-usr-mob"><i className="fa fa-phone-square" aria-hidden="true"></i> {props.phoneNo}</span>
        </p>
      </div>

      <div className="crd-right">
        {!(props.relationId) ?
          <div className="crd-usr-action">
            <i className="fa fa-long-arrow-right" aria-hidden="true"></i>
          </div>
          : <div className="crd-right relation">
            <div className="crd-usr-relation">
              {Relations[props.relationId].LABEL}
            </div>
          </div>
        }
      </div>
    </div>
  );
};



ContactCard.propTypes = {
  onClick: PropTypes.func.isRequired,
  id: PropTypes.number,
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  email: PropTypes.string,
  phoneNo: PropTypes.string,
  relationId: PropTypes.string,
};

export default ContactCard;