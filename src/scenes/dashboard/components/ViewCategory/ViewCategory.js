import React, { Component } from 'react';
import { connect } from 'react-redux';
import SidePanel from '../../../../shared/components/SidePanel/SidePanel';
import { Categories, ContactFilters } from '../../../../actions';
import ContactCard from '../ContactCard/ContactCard';
import SearchBar from '../../../../shared/components/SearchBar/SearchBar';
import { containsStr, equalityCheck } from '../../../../core/utlity';

class ViewCaregory extends Component {

  constructor(props) {
    super(props);
    this.state = {
      filteredContactList: [],
      categoryId: null,
    };
    this.contactList = [];
  }

  componentDidMount() {
    const { location } = this.props;
    const categoryId = location.state && location.state.categoryId ? location.state.categoryId : Categories.UNIDENTIFIED.ID
    this.setState({
      categoryId: categoryId
    });
    this.setContactList(categoryId)
  }

  setContactList = categoryId => {
    this.contactList = (categoryId && this.props.contactList) ?
    this.props.contactList.filter(contact => contact.categoryId === categoryId) : [];
    this.filterContact('')
  };

  filterContact = (constraint) => {
    this.setState({
      filteredContactList: getFilteredList(this.contactList, null, constraint)
    })
  }
  handleCloseClick = () => {
    this.props.history.goBack();
  };

  render() {
    let message = '';
    if (this.state.categoryId) {
      message = `${Categories[this.state.categoryId].LABEL} (${this.state.filteredContactList.length})`;
    }
    let contactListView = this.state.filteredContactList.map((contact, index) => {
      return (
        <ContactCard
          key={index}
          onClick={this.handleContactClick}
          id={contact.id}
          firstName={contact.firstName}
          lastName={contact.lastName}
          email={contact.email}
          phoneNo={contact.phoneNo}
          relationId={contact.relationId}
        />
      );
    });
    if(!contactListView || !contactListView.length){
      contactListView.push(<h2> No result found</h2>)
    }
    return (
      <SidePanel
        closeModal={this.handleCloseClick}
        rightIconClick={this.handleCloseClick}
        headerLabel={message}
      >
        <div className="mng-container">
          <SearchBar
            onSearch={this.filterContact}
          />
          {contactListView}
        </div>
      </SidePanel>
    );
  }
}



const getFilteredList = (contacts, filter, constraint) => {

  switch (filter) {
    case ContactFilters.CATEGORY:
      return contacts.filter(item => containsStr(item.categoryId, constraint) || equalityCheck(item.categoryId, constraint))
    case ContactFilters.FIRST_NAME:
      return contacts.filter(item => containsStr(item.firstName, constraint) || equalityCheck(item.firstName, constraint))
    case ContactFilters.LAST_NAME:
      return contacts.filter(item => containsStr(item.lastName, constraint) || equalityCheck(item.lastName, constraint))
    default:
      return contacts.filter(item => containsStr(item.firstName, constraint) || equalityCheck(item.firstName, constraint))
  }
}

function mapStateToProps(state) {
  return {
    contactList: state.contactList

  };
}

export default connect(
  mapStateToProps,
)(ViewCaregory);
