import React, { Component } from 'react';
import { connect } from 'react-redux';

import SidePanel from '../../../../shared/components/SidePanel/SidePanel';
import ContactCard from '../ContactCard/ContactCard';

import { deepClone } from '../../../../core/utlity';
import { Categories, Relations, saveRelation } from '../../../../actions';
import './styles.scss';
import SaveRelation from './components/SaveRelation/SaveRelation';
class ManageCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contactList: [],
      categoryId: null,
      pageView: 1,
      selectedRelationId: null
    };
    this.selectedContact = null;
  }

  componentDidMount() {
    const { location } = this.props;
    const categoryId = location.state && location.state.categoryId ? location.state.categoryId : Categories.UNIDENTIFIED.ID
    this.setState({
      categoryId: categoryId
    });
    this.setContactList(categoryId)
  }
  getViewTobeRendered = page => {
    let view = [];

    switch (page) {
      case 1:
        
        view.push(<h2 key='hdr'>Click a contact and select who is to you.</h2>);
        let contactListView = this.state.contactList.map((contact, index) => {
          return (
            <ContactCard
              key={index}
              onClick={this.handleContactClick}
              id={contact.id}
              firstName={contact.firstName}
              lastName={contact.lastName}
              email={contact.email}
              phoneNo={contact.phoneNo}
              relationId={contact.relationId}
            />
          );
        });
        view.push(<div className="contact-list">{contactListView}</div>);
        break;
      case 2:
        view.push(
          <SaveRelation
            selectedContact={this.selectedContact}
            selectedRelationId={this.state.selectedRelationId}
            onRelationSelect={this.onRelationSelect}
            saveRelation={this.saveRelation}
          />)
        break;
      default:
        break;
    }
    return view;
  };

  setContactList = categoryId => {
    this.setState({
      contactList : (categoryId && this.props.contactList) ?
      this.props.contactList.filter(contact => contact.categoryId === categoryId) : []
    })
  };

  gotToPage = pageNo => {
    this.setState({
      pageView: pageNo
    });
  };

  saveRelation = () => {
    this.props.saveContactRelation(this.selectedContact.id, this.state.selectedRelationId, Relations[this.state.selectedRelationId].CATEGORY_MAP)
    this.handleCloseClick();
  }


  // Event listeners /////////////
  handleContactClick = id => {
    for (let contact of this.props.contactList) {
      if (contact.id === id) {
        this.selectedContact = contact;
        this.gotToPage(2);
        break;
      }
    }
  };
  onRelationSelect = (relationId) => {
    this.setState({
      selectedRelationId: relationId
    })
  }
  handleBackClick = () => {
    this.gotToPage(1);
  };
  handleCloseClick = () => {
    this.props.history.goBack();
  };

  render() {
    const { pageView } = this.state;
    let message = '';
    if (pageView === 1 && this.state.categoryId) {
      message = `${Categories[this.state.categoryId].LABEL} (${this.state.contactList.length})`;
    } else if (pageView === 2 && this.selectedContact) {
      message = `${this.selectedContact.firstName} ${this.selectedContact.lastName}`;
    }
    return (
      <SidePanel
        closeModal={this.handleCloseClick}
        leftIconClick={pageView === 2 ? this.handleBackClick : null}
        rightIconClick={pageView === 1 ? this.handleCloseClick : null}
        headerLabel={message}
      >
        <div className="mng-container">{this.getViewTobeRendered(pageView)}</div>
      </SidePanel>
    );
  }
}

function mapStateToProps(state) {
  return {
    contactList: state.contactList
  };
}

function mapDispatchToProps(dispatch, ownProps) {
  return {
    saveContactRelation: (contactId, relationId, categoryId) => dispatch(saveRelation(contactId, relationId, categoryId)),
    // setContactList: (contactList) => dispatch(setContactList(contactList))
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ManageCategory);
