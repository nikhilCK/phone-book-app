
import React from "react";
import PropTypes from 'prop-types';

import "./styles.scss"
import Label from '../../../../../../shared/components/Label/Label';
import CardRelation from '../../../CardRelation/CardRelation';
import { Relations } from '../../../../../../actions';
import ButtonSolid from '../../../../../../shared/components/Buttons/ButtonSolid/ButtonSolid';


const SaveRelation = (props) => {

  const { selectedContact, selectedRelationId, onRelationSelect, saveRelation } = props;
  return (
    <div className='relation-page'>
      <div key={'user-details'} className="cnt-crd-details">
        <div className="crd-header">
          Who is '
              <span className="usr-dtl-name">
            {`${selectedContact.firstName} ${selectedContact.lastName}`}
          </span>
          ' to you?
            </div>
        <div className="crd-contain-main">
          <Label text={selectedContact.age.min + ' - ' + selectedContact.age.max} left="AGE" />
          <Label left={<i className="fa fa-map-marker" aria-hidden="true"></i>} text={selectedContact.location} />
        </div>
      </div>
      <label key={'lbl'} className="relation-list-hdr">Select <strong>ONE</strong> of them to help us identify the correct family member</label>
      <CardRelation selectedRelationId={selectedRelationId} relationList={Relations} onClick={onRelationSelect} />
      {
        selectedRelationId ?
          <div className="button-wrpr">
            <ButtonSolid title="Save" onClick={saveRelation} />
          </div>
        : null
      }
    </div>
  );
};



SaveRelation.propTypes = {
  saveRelation: PropTypes.func.isRequired,
  onRelationSelect: PropTypes.func.isRequired,
  selectedContact: PropTypes.object.isRequired,
  selectedRelationId: PropTypes.string
};
export default SaveRelation;