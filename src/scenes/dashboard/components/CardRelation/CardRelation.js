
import React from "react";
import PropTypes from 'prop-types';

import "./styles.scss"


const CardRelation = (props) => {


  const listView = Object.entries(props.relationList).map(([key, relation], index) => {
    return (
      <li key={key} className={(relation.ID === props.selectedRelationId) ? 'selected' : ''} onClick={() => props.onClick(relation.ID)}>
        {relation.LABEL}
      </li>
    )
  })
  return (
    <ul className="relation-options-list">
      {listView}
    </ul>
  );
};



CardRelation.propTypes = {
  onClick: PropTypes.func,
  relationList: PropTypes.object.isRequired,
  selectedRelationId: PropTypes.string
};
CardRelation.defaultProps = {
  isSelected: false,
  relationList: {}
}

export default CardRelation;