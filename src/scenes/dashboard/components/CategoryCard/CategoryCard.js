
import React from "react";
import PropTypes from 'prop-types';

import "./styles.scss"


const CategoryCard = (props) => {

  const onClick = (e) => {
    e.preventDefault();
    e.target.blur();
    props.onClick(props.id);
  };

  return (
    <article className='crd-category accent'>
      <div className="crd-category-lft">
        <div className="crd-category-lft-content">
          <label className="crd-category-count-label">{props.name}</label>
          <div className="crd-category-count">{props.count}</div>
        </div>
      </div>
      <div className="crd-category-right">
        <header className="crd-category-description">{props.description}</header>
        <div className="crd-category-action-label" onClick={onClick}>{props.actionLabel} <i className="fa fa-angle-right" aria-hidden="true"></i></div>
      </div>
    </article>
  );
};



CategoryCard.propTypes = {
  onClick: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  actionLabel: PropTypes.string,
  count: PropTypes.number
};
CategoryCard.defaultProps = {
  count: 0,
  actionLabel : 'Check the list'
};
export default CategoryCard;