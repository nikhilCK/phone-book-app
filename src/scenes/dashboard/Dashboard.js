import React, { Component } from 'react';
import { Route, Switch, Link } from 'react-router-dom';
import { connect } from "react-redux";

import { Categories } from '../../actions';
import { deepClone } from '../../core/utlity';

import ViewCaregory from './components/ViewCategory/ViewCategory';
import ManageCategory from './components/ManageCategory/ManageCategory';
import Phonebook from '../phonebook/Phonebook';

import Pane from '../../shared/components/Pane/Pane';
import ButtonIcon from '../../shared/components/Buttons/ButtonIcon/ButtonIcon';
import ProgressSteps from '../../shared/components/ProgressSteps/ProgressSteps';
import CategoryCard from './components/CategoryCard/CategoryCard';

import './styles.scss';

class Dashboard extends Component {

  componentDidMount(){
    if(!this.props.contactList || !this.props.contactList.length){
      this.props.history.replace('/');
    }
  }
  openModal = (categoryId) =>{
    if(categoryId === Categories.UNIDENTIFIED.ID){
      this.props.history.push('/dashboard/manage/' + categoryId, {
        categoryId : categoryId
      });
    }else {
      this.props.history.push('/dashboard/view/' + categoryId, {
        categoryId : categoryId
      });
    }
    
  }
  goToPhonebook = () =>{
    this.props.history.push('/phonebook');
  }

  render() {
    const { stepsInfo, categoryInfo } = this.props;

    let buttonStyle = {
      width: '100px',
      height: '100px',
      backgroundColor: '#00be6e'
    }
    let iconStyle = {
      color: 'white',
      fontSize: '40px'
    };
    return (
      <main id="dashboard-container" className="container">
        <div className="content">
          <ProgressSteps stepList={stepsInfo.steps} activeStepIndex={stepsInfo.activeStep} />
          <div className="top-sec-panel">
            <div className="title">Import your contacts </div>
            <div className="action"><Link to="/help">Help?</Link></div>
          </div>
          <Pane header={'Choose and view contact relations'}>
            <div>
              {Object.entries(categoryInfo).map(([key, item], index) => {
                return <CategoryCard
                  key={index}
                  id={key}
                  name={item.LABEL}
                  description={item.MSG}
                  actionLabel={item.ACTION}
                  count={item.count}
                  onClick={this.openModal}
                />
              })}
            </div>
          </Pane>
          
          <div className="phn-book-icon">
            <ButtonIcon
                onClick={() => { this.goToPhonebook() }}
                title={'View Phonebook'}
                customStyle={buttonStyle}
                icon={<i className="fa fa-address-book" style={iconStyle} aria-hidden="true" />}
              />
            </div>
          <Switch>
            <Route path="/dashboard/view/:categoryType" component={ViewCaregory} />
            <Route path="/dashboard/manage/:categoryType" component={ManageCategory} />
          </Switch>
        </div>
      </main>
    );
  }
}

const getCategoryInfo = (contactList) => {
  let categoryObj = deepClone(Categories)
  if (contactList) {
    contactList.forEach(contact => {
      if (!categoryObj[contact.categoryId].count) {
        categoryObj[contact.categoryId].count = 0
      }
      categoryObj[contact.categoryId].count++;
    });
  }
  return categoryObj;
}

function mapStateToProps(state) {
  return {
    stepsInfo: state.stepsInfo,
    contactList: state.contactList,
    categoryInfo: getCategoryInfo(state.contactList)
  };
}


export default connect(mapStateToProps)(Dashboard);
