
import React from "react";
import PropTypes from 'prop-types';

import "./styles.scss"
import Label from '../../../../shared/components/Label/Label';
import { Relations } from '../../../../actions';


const ContactItem = (props) => {

  let backgroundColor = props.altr ? 'card-bk-color-2' : 'card-bk-color-1'
  return (
    <div className={"contact-row " + backgroundColor}>
      <div className="left">
        <div className="user_name">{`${props.firstName} ${props.lastName}`} </div>
        {props.relationId ? <Label left={Relations[props.relationId].LABEL}/> : null }
      </div>
      <div className="middle">
        {props.email}
      </div>
      <div className="right">
        {props.phoneNo}
      </div>
    </div>
  );
};


ContactItem.propTypes = {
  firstName: PropTypes.string,
  lastName: PropTypes.string,
  email: PropTypes.string,
  phoneNo: PropTypes.string,
  relationId: PropTypes.string,
  altr: PropTypes.bool
};
ContactItem.defaultProps ={
  atr: false
}

export default ContactItem;