import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from "react-redux";
import { ContactFilters, Categories } from '../../actions';
import { containsStr, equalityCheck, deepClone } from '../../core/utlity';
import SearchBar from '../../shared/components/SearchBar/SearchBar';
import './styles.scss';
import ContactItem from './components/ContactItem/ContactItem';

class Phonebook extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      filteredContactList: [],
      contactCategoryMap: null
    };
    this.contactList = [];
  }

  componentDidMount() {
    if(!this.props.contactList || !this.props.contactList.length){
      this.props.history.replace('/');
      return;
    }

    const newSet = new Map();
    for(const key in Categories){
      newSet.set(key, []);
    }
    this.setState({
      contactCategoryMap : newSet
    },()=>{
      this.filterContact('');
    })
    
  }

  resetView = () =>{
    if(this.state.contactCategoryMap ){
      this.state.contactCategoryMap.forEach((value, key) =>{
        this.state.contactCategoryMap.set(key, [])
      });
    } 
  }
  filterContact = (constraint) => {
    let filteredList = getFilteredList(this.props.contactList, null, constraint);
    this.resetView()
    for(const contact of filteredList){
      this.state.contactCategoryMap.get(contact.categoryId).push(contact)
    }
    this.setState({
      contactCategoryMap :this.state.contactCategoryMap
    })
    console.log(this.state.contactCategoryMap)
  }

  render() {
    const view = []
    if(this.state.contactCategoryMap){
      this.state.contactCategoryMap.forEach((value, key) =>{
        if(value && value.length){
          view.push(<h3> {key} ({value.length}) </h3>)
          view.push(...value.map((contact, index) =>{
            return(
              <ContactItem
                key = {contact.id}
                {...contact}
                altr = {(index % 2)}
              />
            )
          }));
        }
      });
    }
    
    return (
      <main id="phonebook-container" className="container">
        
        <div className="content">
          <header> Contacts </header>
          <SearchBar 
            className="bordered"
            onSearch={this.filterContact}
          />
          <div className="top-sec-panel">
            <div className="title"></div>
            <div className="action"><Link to="/dashboard">Manage List</Link></div>
          </div>
          {
            view
          }
        </div>
      </main>
    )
  }
}


const getFilteredList = (contacts, filter, constraint) => {
  switch (filter) {
    case ContactFilters.CATEGORY:
      return contacts.filter(item => containsStr(item.categoryId, constraint) || equalityCheck(item.categoryId, constraint))
    case ContactFilters.FIRST_NAME:
      return contacts.filter(item => containsStr(item.firstName, constraint) || equalityCheck(item.firstName, constraint))
    case ContactFilters.LAST_NAME:
      return contacts.filter(item => containsStr(item.lastName, constraint) || equalityCheck(item.lastName, constraint))
    default:
      return contacts.filter(item => containsStr(item.firstName, constraint) || equalityCheck(item.firstName, constraint))
  }
}

function mapStateToProps(state) {
  return {
    contactList: state.contactList
  };
}

export default connect(
  mapStateToProps,
)(Phonebook);
