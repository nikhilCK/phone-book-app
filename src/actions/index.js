export const setName = text => ({
  type: 'SET_NAME',
  text
});

export const changeActiveStep = stepIndex =>({
  type: 'CHANGE_ACTIVE_STEP',
  stepIndex
})

export const setContactList = contactList => ({
  type: 'SET_CONTACT_LIST',
  contactList
});

export const saveRelation = (contactId, relationId, categoryId) => ({
  type: 'CHANGE_RELATION',
  contactId,
  relationId,
  categoryId
});

// List of Categories ( set of Categories )
// set of list of contact IDs
/* 
      1. push into list for set
      2. pop from list for set
    */
// List of contacts ( object of contacts )
// list received from api call -> dummy call ->convert to object
/* 
      1. set list
      2. change relation ( relation const)
    */

export const Categories = {
  UNIDENTIFIED: {
    ID: 'UNIDENTIFIED',
    LABEL: 'Unidentified',
    MSG: 'Choose and define contact relationship to you',
    ACTION: 'Choose fron this list'
  },
  FAMAILY: {
    ID: 'FAMAILY',
    LABEL: 'Family Members',
    MSG: 'Your family members are listed here',
    ACTION: 'Check your family members'
  },
  FRIENDS: {
    ID: 'FRIENDS',
    LABEL: 'Friends',
    MSG: 'Your friends are listed here',
    ACTION: 'Check your friends'
  }
};
export const Relations = {
  FATHER: {
    ID: 'FATHER',
    LABEL: 'Father',
    CATEGORY_MAP: 'FAMAILY'
  },
  MOTHER: {
    ID: 'MOTHER',
    LABEL: 'Mother',
    CATEGORY_MAP: 'FAMAILY'
  },
  SISTER: {
    ID: 'SISTER',
    LABEL: 'Sister',
    CATEGORY_MAP: 'FAMAILY'
  },
  BROTHER: {
    ID: 'BROTHER',
    LABEL: 'Brother',
    CATEGORY_MAP: 'FAMAILY'
  },
  SUN: {
    ID: 'SUN',
    LABEL: 'Sun',
    CATEGORY_MAP: 'FAMAILY'
  },
  SPOUSE: {
    ID: 'SPOUSE',
    LABEL: 'Spouse',
    CATEGORY_MAP: 'FAMAILY'
  },
  FRIEND: {
    ID: 'FRIEND',
    LABEL: 'Friend',
    CATEGORY_MAP: 'FRIENDS'
  }
};

export const ContactFilters = {
  CATEGORY: 'CATEGORY',
  FIRST_NAME: 'FIRST_NAME',
  LAST_NAME: 'LAST_NAME',
  EMAIL: 'EMAIL',
  PHONE: 'PHONE'
};