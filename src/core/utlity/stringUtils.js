export const getSearchableStr = (str) => {
  return str.toLowerCase().replace(/ /g, '');
}
export const containsStr = (str1, str2) => {
  return getSearchableStr(str1).includes(getSearchableStr(str2))
}
export const equalityCheck = (str1, str2) => {
  return getSearchableStr(str1) === getSearchableStr(str2)
}