
export const isObject = (value: any) =>
  value !== null && typeof value === 'object';

export const deepClone = (obj: any) => {
  if (obj === null || obj === undefined) {
    return obj;
  } else if (Array.isArray(obj)) {
    // If array, deep clone any mutable elements in the array
    return obj.map(elem => (isObject(elem) ? deepClone(elem) : elem));
  }

  return Object.entries(obj).reduce((acc, [key, value]) => {
    return {
      ...acc,
      [key]: isObject(value) ? deepClone(value) : value
    };
  }, {});
};
