

import React, { Component } from 'react';
import './styles.scss';
export default class HaederBar extends Component {
  render() {
    return (
      <header className="page-header">
        <div className="hdr-content">
            <div className="section-1">
              <span className="title">{this.props.title}</span>
            </div>
          </div>
      </header>
    )
  }
}
