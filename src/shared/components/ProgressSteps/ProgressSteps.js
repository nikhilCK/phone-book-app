
import React from "react";
import PropTypes from 'prop-types';

import "./styles.scss"


const ProgressSteps = (props) => {

  return (

    <div className="progress-step-cntr">
      {
        props.stepList.map((item, index) => {
          let className = "";
          if(index === props.activeStepIndex){
            className = "active"
          }else if(index < props.activeStepIndex){
            className = "visited"
          }
          return (
            <div key={index} className={`step-cntr ${className}`}>
              <span className={`progress-step ${className}`} />
            </div>
          );
        })
      }
    </div>

  );
};



ProgressSteps.propTypes = {
  stepList: PropTypes.array.isRequired,
  activeStepIndex: PropTypes.number.isRequired,
};
ProgressSteps.defaultProps = {
  activeStepIndex: 1
};
export default ProgressSteps;