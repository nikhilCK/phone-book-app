import React, { Component } from 'react';
import './styles.scss';
export default class Pane extends Component {
  render() {
    const {isLoading} = this.props;
    return (
      <div className="panel">
        <header>{this.props.header}</header>
        <section>
          {!isLoading ? this.props.children : null}
          {
            isLoading ? 
            <div className="lds-ring"><div></div><div></div><div></div><div></div></div>
            : null 
          }
        </section>
      </div>

    )
  }
}