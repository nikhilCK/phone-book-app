import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './styles.scss';
export default class SidePanel extends Component {
  render() {
    const { leftIconClick, rightIconClick } = this.props;
    return (
      <div>
        <div className="dialog">
          <div className='dialog-bdy-container' >
            <header className="side-panel-header">
              {leftIconClick ?
                <i onClick={leftIconClick}
                  className={`fa ${this.props.iconLeft}`}
                  aria-hidden="true" />
                : null
              }
              <div className='side-panel-hdr-ttl'>{this.props.headerLabel}</div>
              {rightIconClick ?
                <i onClick={this.props.rightIconClick}
                  className={`fa ${this.props.iconRight}`}
                  aria-hidden="true" />
                : null
              }
            </header>
            {this.props.children}
          </div>
        </div>
        <div className="overlay" onClick={this.props.closeModal} />
      </div>
    );
  }
}

SidePanel.propTypes = {
  closeModal: PropTypes.func,
  iconLeft: PropTypes.string,
  iconRight: PropTypes.string,
  leftIconClick: PropTypes.func,
  rightIconClick: PropTypes.func,
  headerLabel: PropTypes.string
};

SidePanel.defaultProps = {
  iconLeft: 'fa-angle-left',
  iconRight: 'fa-times'
};
